package ee.bcs.valiit.model;

import java.util.ArrayList;
import java.util.List;

public class Dancer {

	private int id;
	private String firstName;
	private String lastName;
	private String phoneNumber;
	private String email;
	private String gender;
	private String memberStatus;
	private float balance;
	private float membershipFee;
	private float trainingFee;
	private String defaultView;
	private String userType;
	private String username;
	private String password;
	private String comment;
	private List<Event> dancerEvents = new ArrayList<>();
	
	public List<Event> getDancerEvents() {
		return dancerEvents;
	}

	public void setDancerEvents(List<Event> dancerEvents) {
		this.dancerEvents = dancerEvents;
	}

	
	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getUserName() {
		return username;
	}

	public void setUserName(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getMemberStatus() {
		return memberStatus;
	}

	public void setMemberStatus(String memberStatus) {
		this.memberStatus = memberStatus;
	}

	public float getBalance() {
		return balance;
	}

	public void setBalance(float balance) {
		this.balance = balance;
	}

	public float getMembershipFee() {
		return membershipFee;
	}

	public void setMembershipFee(float membershipFee) {
		this.membershipFee = membershipFee;
	}

	public float getTrainingFee() {
		return trainingFee;
	}

	public void setTrainingFee(float trainingFee) {
		this.trainingFee = trainingFee;
	}

	public String getDefaultView() {
		return defaultView;
	}

	public void setDefaultView(String defaultView) {
		this.defaultView = defaultView;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

}
