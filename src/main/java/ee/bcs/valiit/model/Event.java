package ee.bcs.valiit.model;

public class Event {
	private int id;
	private String name;
	private float dancerFee;
	private String date;
	private String comment;
	private String participationInfo;
	

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getDancerFee() {
		return dancerFee;
	}

	public void setDancerFee(float dancerFee) {
		this.dancerFee = dancerFee;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	
	public String getParticipationInfo() {
		return participationInfo;
	}

	public void setParticipationInfo(String participationInfo) {
		this.participationInfo = participationInfo;
	}

}
