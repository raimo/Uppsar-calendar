package ee.bcs.valiit.model;

public class QuarterReport {
	
	private int dancer_id;
	private String quarter_date;
	private float quarter_sum;
	private String prepayment;

	
	public int getDancer_id() {
		return dancer_id;
	}
	public void setDancer_id(int dancer_id) {
		this.dancer_id = dancer_id;
	}
	public String getQuarter_date() {
		return quarter_date;
	}
	public void setQuarter_date(String quarter_date) {
		this.quarter_date = quarter_date;
	}
	public float getQuarter_sum() {
		return quarter_sum;
	}
	public void setQuarter_sum(float quarter_sum) {
		this.quarter_sum = quarter_sum;
	}
	public String getPrepayment() {
		return prepayment;
	}
	public void setPrepayment(String prepayment) {
		this.prepayment = prepayment;
	}

}
