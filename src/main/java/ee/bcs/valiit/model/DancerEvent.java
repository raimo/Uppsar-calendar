package ee.bcs.valiit.model;

public class DancerEvent {

	private int dancerId;
	private int eventId;
	private String didAttend;
	private int replacementDancerId;
	private String participationInfo;

	public String getParticipationInfo() {
		return participationInfo;
	}

	public void setParticipationInfo(String participationInfo) {
		this.participationInfo = participationInfo;
	}

	public int getDancerId() {
		return dancerId;
	}

	public void setDancerId(int dancerId) {
		this.dancerId = dancerId;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public String isDidAttend() {
		return didAttend;
	}

	public void setDidAttend(String didAttend) {
		this.didAttend = didAttend;
	}

	public int getReplacementDancerId() {
		return replacementDancerId;
	}

	public void setReplacementDancerId(int replacementDancerId) {
		this.replacementDancerId = replacementDancerId;
	}
	
}
