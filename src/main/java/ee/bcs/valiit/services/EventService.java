package ee.bcs.valiit.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import ee.bcs.valiit.model.Event;

public class EventService {
	public static final String SQL_CONNECTION_URL = "jdbc:mysql://localhost:3306/dancers";
	public static final String SQL_USERNAME = "root";
	public static final String SQL_PASSWORD = "tere";

	public static ResultSet executeSQL(String sql) {
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			try (Connection conn = DriverManager.getConnection(SQL_CONNECTION_URL, SQL_USERNAME, SQL_PASSWORD)) {
				try (Statement stmt = conn.createStatement()) {
					return stmt.executeQuery(sql);
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<Event> getEvents() {
		List<Event> events = new ArrayList<Event>(); 
		try {
			ResultSet result = executeSQL("select * from dancers.event ORDER BY event.eventDate ASC, event.id ASC");
			if (result != null) {
				while (result.next()) {
					Event event = new Event();
					event.setId(result.getInt("id"));
					event.setName(result.getString("eventName"));
					event.setDate(result.getString("eventDate"));
					event.setDancerFee(result.getFloat("dancerFee"));
					event.setComment(result.getString("comment"));
					events.add(event);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return events;
	}

	public static List<Event> getEventsInDateRange(String startDate, String endDate ) {
		List<Event> events = new ArrayList<Event>(); 
		try { 
			ResultSet result = executeSQL("select * from dancers.event where event.eventDate >'" + startDate + "-01' AND event.eventDate < '"+  endDate +"-01' ORDER BY event.eventDate ASC, event.id ASC");
			if (result != null) {
				while (result.next()) {
					Event event = new Event();
					event.setId(result.getInt("id"));
					event.setName(result.getString("eventName"));
					event.setDate(result.getString("eventDate"));
					event.setDancerFee(result.getFloat("dancerFee"));
					event.setComment(result.getString("comment"));
					events.add(event);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return events;
	}

	public static void addEvent(Event event) {
		// Salvestame ürituse andmebaasi
		String sql = "INSERT INTO dancers.event (eventName, dancerFee, eventDate, comment) VALUES ('%s', %s, '%s', '%s')";
		sql = String.format(sql, event.getName(), event.getDancerFee(), event.getDate(), event.getComment());
		executeSQL(sql);

	}

	public static void removeEvent(int eventId) {
		String sql = "DELETE FROM dancers.event WHERE id = " + eventId;
		executeSQL(sql);
	}

	public static Event getEvent(int eventId) {
		try {
			String sql = "select * from dancers.event where id=" + eventId;
			ResultSet result = executeSQL(sql);
			if (result != null) {
				if (result.next()) { // kuni saan liikuda järgmisele reale
					Event event = new Event(); // loon iga kord objekti ja määran selle parameetritele
													// väärtused
					
					event.setId(result.getInt("id"));
					event.setName(result.getString("eventName"));
					event.setDate(result.getString("eventDate"));
					event.setDancerFee(result.getFloat("dancerFee"));
					event.setComment(result.getString("comment"));
					return event;
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void editEvent(Event event) {
		String sql = String.format(
				"UPDATE dancers.event SET eventName ='%s', dancerFee =%s, eventDate ='%s', comment ='%s' WHERE id=%s",
				event.getName(), event.getDancerFee(), event.getDate(), event.getComment(), event.getId());
		executeSQL(sql);
	}
	
	
	
	
	
	
//	public static String isWorkingAllowed(String testDate) {
//		Calendar myCalendar = Calendar.getInstance();
//		Map<String, Integer> myDateParts = deriveDateParts (testDate);
//		myCalendar.set(myDateParts.get("year"), myDateParts.get("month"), myDateParts.get("day"));
//		
// 		
//	}
//
//	private static Map<String, Integer> deriveDateParts(String testDate) {
//		Map<String, Integer>dateParts = new HashMap<>();
//		String [] dateComponents = testDate.split("-");
//		dateParts.put("year", Integer.parseInt(dateComponents[0]));
//		dateParts.put("month", Integer.parseInt(dateComponents[1]));
//		dateParts.put("day", Integer.parseInt(dateComponents[2]));
//		
//		return dateParts;
//	}
}
