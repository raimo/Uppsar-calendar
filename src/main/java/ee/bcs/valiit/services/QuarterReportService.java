package ee.bcs.valiit.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import ee.bcs.valiit.model.Event;
import ee.bcs.valiit.model.QuarterReport;

public class QuarterReportService {
	public static final String SQL_CONNECTION_URL = "jdbc:mysql://localhost:3306/dancers";
	public static final String SQL_USERNAME = "root";
	public static final String SQL_PASSWORD = "tere";

	public static ResultSet executeSQL(String sql) {
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			try (Connection conn = DriverManager.getConnection(SQL_CONNECTION_URL, SQL_USERNAME, SQL_PASSWORD)) {
				try (Statement stmt = conn.createStatement()) {
					return stmt.executeQuery(sql);
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void addQuarterReport(QuarterReport quarterReport) {
		String sql = "INSERT INTO dancers.quarter_report (dancer_id, quarter_date, quarter_sum, prepayment) VALUES (%s, '%s', %s, '%s')";
		sql = String.format(sql, quarterReport.getDancer_id(), quarterReport.getQuarter_date(),
				quarterReport.getQuarter_sum(), quarterReport.getPrepayment());
		executeSQL(sql);

	}

	public static void updateQuarterReport(QuarterReport r) {
		String sql = "UPDATE dancers.quarter_report SET quarter_sum=%s, prepayment='%s' WHERE dancer_id=%s AND quarter_date = '%s'";
		sql = String.format(sql, r.getQuarter_sum(), r.getPrepayment(), r.getDancer_id(), r.getQuarter_date());
		executeSQL(sql);

	}

	public static QuarterReport getThisQuarterReport(int dancer_id, String quarter_date) {

		try {
			String sql = "SELECT * FROM dancers.quarter_report WHERE dancer_id=%s AND quarter_date = '%s'";
			sql = String.format(sql, dancer_id, quarter_date);
			ResultSet result = executeSQL(sql);
			if (result != null) {
				if (result.next()) {
				QuarterReport quarterReport = new QuarterReport();
				quarterReport.setDancer_id(dancer_id);
				quarterReport.setQuarter_date(quarter_date);
				quarterReport.setQuarter_sum(result.getFloat("quarter_sum"));
				quarterReport.setPrepayment(result.getString("prepayment"));
				return quarterReport;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();

		}
		return null;

	}

}
