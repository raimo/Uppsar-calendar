package ee.bcs.valiit.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ee.bcs.valiit.model.Dancer;
import ee.bcs.valiit.model.DancerEvent;
import ee.bcs.valiit.model.Event;

public class DancerEventService {
	public static final String SQL_CONNECTION_URL = "jdbc:mysql://localhost:3306/dancers";
	public static final String SQL_USERNAME = "root";
	public static final String SQL_PASSWORD = "tere";

	public static ResultSet executeSQL(String sql) {
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			try (Connection conn = DriverManager.getConnection(SQL_CONNECTION_URL, SQL_USERNAME, SQL_PASSWORD)) {
				try (Statement stmt = conn.createStatement()) {
					return stmt.executeQuery(sql);
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<DancerEvent> getDancerEvents() {
		List<DancerEvent> dancerEvents = new ArrayList<DancerEvent>();
		try {
			ResultSet result = executeSQL("select * from dancers.dancer_event");
			if (result != null) {
				while (result.next()) {
					DancerEvent dancerEvent = new DancerEvent();
					dancerEvent.setDancerId(result.getInt("dancer_id"));
					dancerEvent.setEventId(result.getInt("event_id"));
					dancerEvent.setDidAttend(result.getString("didAttend"));
					dancerEvent.setReplacementDancerId(result.getInt("replacementDancer"));
					dancerEvent.setParticipationInfo(result.getString("participationInfo"));
					dancerEvents.add(dancerEvent);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dancerEvents;
	}

	public static DancerEvent getDancerEvent(int eventId, int dancerId) {
		try {
			String sql = "select * from dancers.dancer_event where event_id=" + eventId + " AND dancer_id=" + dancerId;
			ResultSet result = executeSQL(sql);
			DancerEvent dancerEvent = new DancerEvent();
			if (result != null) {
				if (result.next()) {
					dancerEvent.setDancerId(result.getInt("dancer_id"));
					dancerEvent.setEventId(result.getInt("event_id"));
					dancerEvent.setParticipationInfo(result.getString("participationInfo"));
					dancerEvent.setReplacementDancerId(result.getInt("replacementDancer"));
					return dancerEvent;

				} else {
					dancerEvent.setDancerId(dancerId);
					dancerEvent.setEventId(eventId);
					dancerEvent.setParticipationInfo("");
					dancerEvent.setReplacementDancerId(0);
					return dancerEvent;
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void addDancerEvent(int dancer_id, int event_id, String participationInfo, int replacementDancerId) {
		String sql2 = "select * from dancers.dancer_event WHERE dancer_id =" + dancer_id + " AND event_id=" + event_id;
		ResultSet result = executeSQL(sql2);

		try {
			if (result == null || !result.next()) {
				String sql = String.format(
						"INSERT INTO dancers.dancer_event (dancer_id, event_id, replacementDancer, participationInfo) VALUES (%s, %s,%s,'%s')",
						dancer_id, event_id, replacementDancerId, participationInfo);
				executeSQL(sql);

			} else {
				String sql = String.format(
						"UPDATE dancers.dancer_event SET replacementDancer=%s, participationInfo='%s' WHERE dancer_id=%s AND event_id=%s",
						replacementDancerId, participationInfo, dancer_id, event_id);
				executeSQL(sql);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public static List<Event> getDancerEventsInDateRange(int dancerId, int fromYear, int fromMonth, int toYear,
			int toMonth) {
		String fromDate = String.format("%s-%s-%s", fromYear, fromMonth, "01");
		String toDate = String.format("%s-%s-%s", toYear, toMonth, "01");

		String sql = String.format(
				"SELECT * FROM event LEFT JOIN dancer_event ON event.id = dancer_event.event_id AND dancer_event.dancer_id =  %s WHERE eventDate >= '%s' AND eventDate < '%s' ORDER BY event.eventDate ASC, event.id ASC",
				dancerId, fromDate, toDate);

		List<Event> dancerEvents = new ArrayList<Event>();
		try {
			ResultSet result = executeSQL(sql);
			if (result != null) {
				while (result.next()) {
					Event event = new Event();
					event.setId(result.getInt("id"));
					event.setName(result.getString("eventName"));
					event.setDate(result.getString("eventDate"));
					event.setDancerFee(result.getFloat("dancerFee"));
					event.setComment(result.getString("comment"));
					event.setParticipationInfo(
							result.getString("participationInfo") != null ? result.getString("participationInfo")
									: "NULL");
					dancerEvents.add(event);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dancerEvents;
	}

	public static List<Dancer> getDancersDate(int fromYear, int fromMonth, int toYear, int toMonth) {

		List<Dancer> dancers = new ArrayList<Dancer>(); // loon listi, mis koosneb objektisest, mille tüüp on
		// Dancer
		try {
			ResultSet result = executeSQL("select * from dancers.dancer");
			if (result != null) {
				while (result.next()) { // kuni saan liikuda järgmisele reale
					Dancer dancer = new Dancer(); // loon iga kord Dancer objekti ja määran selle parameetritele
					// väärtused
					dancer.setId(result.getInt("id"));
					dancer.setFirstName(result.getString("firstName"));
					dancer.setLastName(result.getString("lastName"));
					dancer.setPhoneNumber(result.getString("phoneNumber"));
					dancer.setEmail(result.getString("email"));
					dancer.setGender(result.getString("gender"));
					dancer.setMemberStatus(result.getString("memberStatus"));
					dancer.setBalance(result.getFloat("balance"));
					dancer.setMembershipFee(result.getFloat("membershipFee"));
					dancer.setTrainingFee(result.getFloat("trainingFee"));
					dancer.setDefaultView(result.getString("defaultView"));
					dancer.setUserName(result.getString("username"));

					dancer.setPassword(result.getString("password"));
					dancer.setUserType(result.getString("userType"));
					dancer.setComment(result.getString("comment"));

					List<Event> dancerEvents = getDancerEventsInDateRange(dancer.getId(), fromYear, fromMonth, toYear,
							toMonth);
					for (Event ev : dancerEvents) {
						dancer.getDancerEvents().add(ev);
					}

					dancers.add(dancer); // lisan loodud objekti dancers listi (objektidest)

				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dancers;

	}

}
