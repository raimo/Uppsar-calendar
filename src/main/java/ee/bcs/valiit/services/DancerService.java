package ee.bcs.valiit.services;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ee.bcs.valiit.model.Dancer;
import ee.bcs.valiit.model.Event;

public class DancerService {
	public static final String SQL_CONNECTION_URL = "jdbc:mysql://localhost:3306/dancers";
	public static final String SQL_USERNAME = "root";
	public static final String SQL_PASSWORD = "tere";

	public static ResultSet executeSQL(String sql) {
		try {
			Class.forName("org.mariadb.jdbc.Driver");
			try (Connection conn = DriverManager.getConnection(SQL_CONNECTION_URL, SQL_USERNAME, SQL_PASSWORD)) {
				try (Statement stmt = conn.createStatement()) {
					return stmt.executeQuery(sql);
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static List<Dancer> getDancers() {
		List<Dancer> dancers = new ArrayList<Dancer>(); // loon listi, mis koosneb objektisest, mille tüüp on
														// Dancer
		try {
			ResultSet result = executeSQL("select * from dancers.dancer ORDER BY dancers.dancer.gender ASC, dancers.dancer.firstName ASC, dancers.dancer.lastName ASC");
			if (result != null) {
				while (result.next()) { // kuni saan liikuda järgmisele reale
					Dancer dancer = new Dancer(); // loon iga kord Dancer objekti ja määran selle parameetritele
													// väärtused
					dancer.setId(result.getInt("id"));
					dancer.setFirstName(result.getString("firstName"));
					dancer.setLastName(result.getString("lastName"));
					dancer.setPhoneNumber(result.getString("phoneNumber"));
					dancer.setEmail(result.getString("email"));
					dancer.setGender(result.getString("gender"));
					dancer.setMemberStatus(result.getString("memberStatus"));
					dancer.setBalance(result.getFloat("balance"));
					dancer.setMembershipFee(result.getFloat("membershipFee"));
					dancer.setTrainingFee(result.getFloat("trainingFee"));
					dancer.setDefaultView(result.getString("defaultView"));
					dancer.setUserName(result.getString("username"));
					dancer.setPassword(result.getString("password"));
					dancer.setUserType(result.getString("userType"));
					dancer.setComment(result.getString("comment"));
					dancers.add(dancer); // lisan loodud objekti dancers listi (objektidest)
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return dancers;
	}

	public static void addDancer(Dancer dancer) {
		// Salvestame tantsija andmebaasi
		String sql = "INSERT INTO dancers.dancer (firstName, lastName, phoneNumber, email, gender, memberStatus, balance, membershipFee, trainingFee, defaultView, username, password, userType, comment) VALUES ('%s', '%s', '%s', '%s','%s', '%s', %s, %s, %s,'%s', '%s', '%s', '%s', '%s')";
		sql = String.format(sql, dancer.getFirstName(), dancer.getLastName(), dancer.getPhoneNumber(),
				dancer.getEmail(), dancer.getGender(), dancer.getMemberStatus(), dancer.getBalance(),
				dancer.getMembershipFee(), dancer.getTrainingFee(), dancer.getDefaultView(), dancer.getUserName(),
				dancer.getPassword(), dancer.getUserType(), dancer.getComment());
		executeSQL(sql);

	}

	public static void removeDancer(int dancerId) {
		String sql = "DELETE FROM dancers.dancer WHERE id = " + dancerId;
		executeSQL(sql);
	}

	public static Dancer getDancer(int dancerId) {
		try {
			String sql = "select * from dancers.dancer where id=" + dancerId;
			ResultSet result = executeSQL(sql);
			if (result != null) {
				if (result.next()) { // kuni saan liikuda järgmisele reale
					Dancer dancer = new Dancer(); // loon iga kord Company objekti ja määran selle parameetritele
													// väärtused
					dancer.setId(result.getInt("id"));
					dancer.setFirstName(result.getString("firstName"));
					dancer.setLastName(result.getString("lastName"));
					dancer.setPhoneNumber(result.getString("phoneNumber"));
					dancer.setEmail(result.getString("email"));
					dancer.setGender(result.getString("gender"));
					dancer.setMemberStatus(result.getString("memberStatus"));
					dancer.setBalance(result.getFloat("balance"));
					dancer.setTrainingFee(result.getFloat("trainingFee"));
					dancer.setMembershipFee(result.getFloat("membershipFee"));
					dancer.setDefaultView(result.getString("defaultView"));
					dancer.setUserName(result.getString("username"));
					dancer.setPassword(result.getString("password"));
					dancer.setUserType(result.getString("userType"));
					dancer.setComment(result.getString("comment"));

					return dancer;
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void editDancer(Dancer dancer) {
		String sql = String.format(
				"UPDATE dancers.dancer SET firstName ='%s', lastName ='%s', phoneNumber ='%s', email ='%s', gender ='%s', memberStatus ='%s', balance =%s, membershipFee=%s, trainingFee=%s, defaultView ='%s', username ='%s', password ='%s', userType ='%s', comment='%s' WHERE id=%s",
				dancer.getFirstName(), dancer.getLastName(), dancer.getPhoneNumber(), dancer.getEmail(),
				dancer.getGender(), dancer.getMemberStatus(), dancer.getBalance(), dancer.getMembershipFee(),
				dancer.getTrainingFee(), dancer.getDefaultView(), dancer.getUserName(), dancer.getPassword(),
				dancer.getUserType(), dancer.getComment(), dancer.getId());
		executeSQL(sql);
	}

	


	// public static String isWorkingAllowed(String testDate) {
	// Calendar myCalendar = Calendar.getInstance();
	// Map<String, Integer> myDateParts = deriveDateParts (testDate);
	// myCalendar.set(myDateParts.get("year"), myDateParts.get("month"),
	// myDateParts.get("day"));
	//
	//
	// }
	//
	// private static Map<String, Integer> deriveDateParts(String testDate) {
	// Map<String, Integer>dateParts = new HashMap<>();
	// String [] dateComponents = testDate.split("-");
	// dateParts.put("year", Integer.parseInt(dateComponents[0]));
	// dateParts.put("month", Integer.parseInt(dateComponents[1]));
	// dateParts.put("day", Integer.parseInt(dateComponents[2]));
	//
	// return dateParts;
	// }
}
