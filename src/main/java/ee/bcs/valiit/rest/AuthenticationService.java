package ee.bcs.valiit.rest;

import java.sql.ResultSet;
import java.sql.SQLException;

import ee.bcs.valiit.model.Dancer;
import ee.bcs.valiit.services.DancerService;

public class AuthenticationService {

	public static Dancer getUser(String username, String password) {
		String sql = String.format("SELECT * FROM dancer WHERE username= '%s' AND password = '%s'", username, password);
		ResultSet userRecord = DancerService.executeSQL(sql);
		try {
			if (userRecord != null && userRecord.next()) {
				Dancer dancer = new Dancer();
				dancer.setId(userRecord.getInt("id"));
				dancer.setFirstName(userRecord.getString("firstName"));
				dancer.setLastName(userRecord.getString("lastName"));
				dancer.setPhoneNumber(userRecord.getString("phoneNumber"));
				dancer.setEmail(userRecord.getString("email"));
				dancer.setGender(userRecord.getString("gender"));
				dancer.setMemberStatus(userRecord.getString("memberStatus"));
				dancer.setBalance(userRecord.getFloat("balance"));
				dancer.setTrainingFee(userRecord.getFloat("trainingFee"));
				dancer.setMembershipFee(userRecord.getFloat("membershipFee"));
				dancer.setDefaultView(userRecord.getString("defaultView"));
				dancer.setUserName(userRecord.getString("username"));
				dancer.setUserType(userRecord.getString("userType"));
				dancer.setComment(userRecord.getString("comment"));
				return dancer;
			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		return null;
	}

}
