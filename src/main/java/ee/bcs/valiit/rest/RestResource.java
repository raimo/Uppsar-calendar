package ee.bcs.valiit.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import ee.bcs.valiit.model.Dancer;
import ee.bcs.valiit.model.DancerEvent;
import ee.bcs.valiit.model.Event;
import ee.bcs.valiit.model.QuarterReport;
import ee.bcs.valiit.services.DancerEventService;
import ee.bcs.valiit.services.DancerService;
import ee.bcs.valiit.services.EventService;
import ee.bcs.valiit.services.QuarterReportService;

@Path("/")
public class RestResource {

	// - - - - - - - - - - - - - - - - - DANCER REST METHODS - - - - - - - - - - - -
	// - -

	@GET
	@Path("/get_dancers")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Dancer> getDancers(@Context HttpServletRequest req) {
		if (isUserAuthorized(req, "A") || isUserAuthorized(req, "M") || isUserAuthorized(req, "U")) {
			return DancerService.getDancers();
		}
		return new ArrayList<>();
	}

	@GET
	@Path("/get_dancers_with_period_events")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Dancer> getDancersWithPeriodEvents(@Context HttpServletRequest req,
			@QueryParam("from_year") int fromYear, @QueryParam("from_month") int fromMonth,
			@QueryParam("to_year") int toYear, @QueryParam("to_month") int toMonth) {

		if (isUserAuthorized(req, "A") || isUserAuthorized(req, "M") || isUserAuthorized(req, "U")) {
			List<Dancer> dancers = DancerService.getDancers();
			for (Dancer dancer : dancers) {
				dancer.getDancerEvents().addAll(DancerEventService.getDancerEventsInDateRange(dancer.getId(), fromYear,
						fromMonth, toYear, toMonth));
			}
			return dancers;
		}
		return new ArrayList<>();
	}

	@POST // muudab meetodid veebis "nähtavaks", ehk saan browseri kaudu käivitada
	@Path("/add_dancer")
	@Consumes(MediaType.APPLICATION_JSON) // võtab JSON struktuuris infot
	@Produces(MediaType.TEXT_PLAIN) // tagastab stringi kujul teksti
	public String addDancer(@Context HttpServletRequest req, Dancer Dancer) {
		if (isUserAuthorized(req, "A") || isUserAuthorized(req, "M")) {
			DancerService.addDancer(Dancer);
			return "OK";
		}
		return "";
	}

	@GET
	@Path("/get_dancer")
	@Produces(MediaType.APPLICATION_JSON)
	public Dancer getDancer(@Context HttpServletRequest req, @QueryParam("dancer_id") int id) { // saame parameetri
																								// URL-ina
		if (isUserAuthorized(req, "A") || isUserAuthorized(req, "M") || isUserAuthorized(req, "U")) {
			return DancerService.getDancer(id);
		}
		return null;
	}

	@POST // muudab meetodid veebis "nähtavaks", ehk saan browseri kaudu käivitada
	@Path("/edit_dancer")
	@Consumes(MediaType.APPLICATION_JSON) // võtab JSON struktuuris infot
	@Produces(MediaType.TEXT_PLAIN) // tagastab stringi kujul teksti
	public String editDancer(@Context HttpServletRequest req, Dancer Dancer) {
		if (isUserAuthorized(req, "A") || isUserAuthorized(req, "M")) {
			DancerService.editDancer(Dancer);
			return "OK";
		}
		return "";
	}

	@POST
	@Path("/remove_dancer")
	@Produces(MediaType.TEXT_PLAIN)
	public String removeDancer(@Context HttpServletRequest req, @FormParam("dancer_id") int id) {
		if (isUserAuthorized(req, "A") || isUserAuthorized(req, "M")) {
			DancerService.removeDancer(id);
			return "OK";
		}
		return "";
	}

	// - - - - - - - - - - - - - - - - - EVENT REST METHODS - - - - - - - - - - - -
	// - -

	// @POST
	// @Path("/get_date")
	// @Produces(MediaType.APPLICATION_JSON)
	// public String getDate (@FormParam("event_date") String date) {
	// return "OK";
	// }

	@GET
	@Path("/get_events")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Event> getEvents(@Context HttpServletRequest req) {
		if (isUserAuthorized(req, "A") || isUserAuthorized(req, "M") || isUserAuthorized(req, "U")) {
			return EventService.getEvents();
		}
		return new ArrayList<>();
	}

	@GET
	@Path("/get_events_in_date_range")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Event> getEventsInDateRange(@Context HttpServletRequest req, @QueryParam("startDate") String startDate,
			@QueryParam("endDate") String endDate) {
		if (isUserAuthorized(req, "A") || isUserAuthorized(req, "M") || isUserAuthorized(req, "U")) {
			return EventService.getEventsInDateRange(startDate, endDate);
		}
		return new ArrayList<>();
	}

	@POST
	@Path("/add_event")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String addEvent(@Context HttpServletRequest req, Event Event) {
		if (isUserAuthorized(req, "A") || isUserAuthorized(req, "M")) {
			EventService.addEvent(Event);
			return "OK";
		}
		return "";
	}

	@GET
	@Path("/get_event")
	@Produces(MediaType.APPLICATION_JSON)
	public Event getEvent(@Context HttpServletRequest req, @QueryParam("event_id") int id) {
		if (isUserAuthorized(req, "A") || isUserAuthorized(req, "M") || isUserAuthorized(req, "U")) {
			return EventService.getEvent(id);
		}
		return null;
	}

	@POST
	@Path("/edit_event")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String editEvent(@Context HttpServletRequest req, Event Event) {
		if (isUserAuthorized(req, "A") || isUserAuthorized(req, "M")) {
			EventService.editEvent(Event);
			return "OK";
		}
		return "";
	}

	@POST
	@Path("/remove_event")
	@Produces(MediaType.TEXT_PLAIN)
	public String removeEvent(@Context HttpServletRequest req, @FormParam("event_id") int id) {
		if (isUserAuthorized(req, "A") || isUserAuthorized(req, "M")) {
			EventService.removeEvent(id);
			return "OK";
		}
		return "";
	}

	@GET
	@Path("/get_dancerEventsInDateRange")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Event> getDancerEventsInDateRange(@Context HttpServletRequest req,
			@QueryParam("dancer_id") int dancerId, @QueryParam("from_year") int fromYear,
			@QueryParam("from_month") int fromMonth, @QueryParam("to_year") int toYear,
			@QueryParam("to_month") int toMonth) {
		if (isUserAuthorized(req, "A") || isUserAuthorized(req, "M") || isUserAuthorized(req, "U")) {
			return DancerEventService.getDancerEventsInDateRange(dancerId, fromYear, fromMonth, toYear, toMonth);
		}
		return new ArrayList<>();
	}

	@GET
	@Path("/get_dancerEvents")
	@Produces(MediaType.APPLICATION_JSON)
	public List<DancerEvent> getDancerEvents() {
		return DancerEventService.getDancerEvents();
	}

	@POST
	@Path("/add_dancerEvent")
	@Produces(MediaType.TEXT_PLAIN)
	public String addDancerEvent(@Context HttpServletRequest req, @FormParam("dancer_id") int dancer_id,
			@FormParam("event_id") int event_id, @FormParam("participationInfo") String participationInfo,
			@FormParam("replacementDancerId") int replacementDancerId) {
		if (isUserAuthorized(req, "A") || isUserAuthorized(req, "M")) {
			DancerEventService.addDancerEvent(dancer_id, event_id, participationInfo, replacementDancerId);
			return "OK";
		}
		return "";
	}

	@GET
	@Path("/get_dancerevent")
	@Produces(MediaType.APPLICATION_JSON)
	public DancerEvent getDancerEvent(@Context HttpServletRequest req, @QueryParam("event_id") int eventId,
			@QueryParam("user_id") int dancerId) {
		if (isUserAuthorized(req, "A") || isUserAuthorized(req, "M") || isUserAuthorized(req, "U")) {
			return DancerEventService.getDancerEvent(eventId, dancerId);
		}
		return null;

	}

	// - - - - - - - - - - - - - - REPORT - - - - - - - - - - - - - -

	@POST
	@Path("/update_quarterReports")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String updateQuarterReport(@Context HttpServletRequest req, QuarterReport[] quarterReports) {
		if (isUserAuthorized(req, "A") || isUserAuthorized(req, "M") || isUserAuthorized(req, "U")) {
			for (QuarterReport r : quarterReports) {

				if (QuarterReportService.getThisQuarterReport(r.getDancer_id(), r.getQuarter_date()) != null) {
					QuarterReportService.updateQuarterReport(r);
				} else {
					QuarterReportService.addQuarterReport(r);
				}
			}
			return "OK";
		}
		return "";
	}

	@GET
	@Path("/get_dancerQuarterReport")
	@Produces(MediaType.APPLICATION_JSON)
	public QuarterReport getDancerQuarterReport(@Context HttpServletRequest req,
			@QueryParam("quarterDate") String quarterDate, @QueryParam("user_id") int dancerId) {
		if (isUserAuthorized(req, "A") || isUserAuthorized(req, "M") || isUserAuthorized(req, "U")) {

			return QuarterReportService.getThisQuarterReport(dancerId, quarterDate);
		}
		return null;
	}

	@GET
	@Path("/get_dancerQuarterReports")
	@Produces(MediaType.APPLICATION_JSON)
	public QuarterReport[] getDancerQuarterReport(@Context HttpServletRequest req,
			@QueryParam("currentQuarterDate") String currentQuarterDate,
			@QueryParam("previousQuarterDate") String previousQuarterDate, @QueryParam("user_id") int dancerId) {
		if (isUserAuthorized(req, "A") || isUserAuthorized(req, "M") || isUserAuthorized(req, "U")) {
			QuarterReport previousReport = QuarterReportService.getThisQuarterReport(dancerId, previousQuarterDate);
			QuarterReport currentReport = QuarterReportService.getThisQuarterReport(dancerId, currentQuarterDate);

			return new QuarterReport[] { previousReport, currentReport };
		}
		return null;

	}

	/*
	 * 
	 * LET*S MAKE A COOKIE
	 * 
	 * @GET
	 * 
	 * @Path("/set_cookie")
	 * 
	 * @Produces(MediaType.TEXT_PLAIN) public Response setCookie() { NewCookie
	 * myCookie = new NewCookiw("MY_TEST_COOKIE", "ABC123", "/", null, null, -1,
	 * false, false); return
	 * Response.ok("Cookie set successfully!").cookie(myCookie).build(); }
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * @GET
	 * 
	 * @Path("/read_cookie")
	 * 
	 * @Produces(MediaType.TEXT_PLAIN) public String
	 * readCookie(@CookieParam("MY_TEST_COOKIE") String myCookie) { return
	 * "The cookie value was " myCookie; }
	 * 
	 * @GET
	 * 
	 * @Path("/set_session_info")
	 * 
	 * @Produces(MediaType.TEXT_PLAIN) public String setSessionInfo(@Context
	 * HttpServletRequest req){ //TomCat server info läheb kaasa HttpSession session
	 * = req.getSession(true); //sessioniga seotud info String myTestAttr =
	 * (String)session.getAttribute("TEST"); if (myTestAttr != null){ return
	 * "Session attribute found: " + myTestAttr; } else { //Session attribute was
	 * not found. session.setAttribute("TEST", String.valueof(Math.random()); return
	 * "Session attribute generated: " + (String)session.getAttribute("TEST"); } }
	 * 
	 * 
	 * 
	 */
	@POST
	@Path("/authenticate_user")
	@Produces(MediaType.TEXT_PLAIN)
	public String authenticateUser(@Context HttpServletRequest req, @FormParam("username") String username,
			@FormParam("password") String password) {

		Dancer dancer = AuthenticationService.getUser(username, password);

		if (dancer == null) {
			return "Sisselogimine ebaõnnestus";
		} else {
			HttpSession session = req.getSession(true);
			session.setAttribute("AUTH_USER", dancer);
			return "SUCCESS";
		}
	}

	@GET
	@Path("get_authenticated_user")
	@Produces(MediaType.APPLICATION_JSON)
	public Dancer getAuthenticatedUser(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		if (session.getAttribute("AUTH_USER") != null) {
			// Kasutaja sisse loginud
			return (Dancer) session.getAttribute("AUTH_USER");
		} else {
			// Kasutaja ei ole sisse loginud
			return new Dancer(); // tagastab tühja objekti
		}
	}

	@GET
	@Path("/logout")
	@Produces(MediaType.TEXT_PLAIN)
	public String logout(@Context HttpServletRequest req) {
		HttpSession session = req.getSession(true);
		session.removeAttribute("AUTH_USER");
		return "Välja logitud!";
	}

	private boolean isUserAuthorized(@Context HttpServletRequest req, String expectedRole) {
		HttpSession session = req.getSession(true);
		Dancer dancer = null;
		if (session.getAttribute("AUTH_USER") != null) {
			// Kasutaja sisse loginud
			dancer = (Dancer) session.getAttribute("AUTH_USER");
			return dancer.getUserType().equals(expectedRole);
		}
		return false;
	}
}
