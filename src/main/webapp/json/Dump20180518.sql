CREATE DATABASE  IF NOT EXISTS `dancers` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `dancers`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: dancers
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dancer`
--

DROP TABLE IF EXISTS `dancer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dancer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `phoneNumber` varchar(45) NOT NULL,
  `email` varchar(120) NOT NULL,
  `gender` enum('MALE','FEMALE') NOT NULL,
  `memberStatus` enum('ACTIVE','INACTIVE') NOT NULL,
  `balance` decimal(7,2) DEFAULT NULL,
  `membershipFee` decimal(7,2) DEFAULT NULL,
  `trainingFee` decimal(7,2) DEFAULT NULL,
  `defaultView` varchar(45) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `userType` enum('A','M','U') NOT NULL DEFAULT 'U',
  `comment` varchar(225) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dancer`
--

LOCK TABLES `dancer` WRITE;
/*!40000 ALTER TABLE `dancer` DISABLE KEYS */;
INSERT INTO `dancer` VALUES (20,'Juku','Tamm','55516846','juku.tamm@gmail.com','MALE','ACTIVE',123.00,123.00,123.00,'aasdf','JukuT','aaa','A','sdfdsaf'),(22,'Tiina','Tina','3123','Tiina.Tina@eesti.ee','FEMALE','ACTIVE',121.00,12.10,1.11,'12','','123','U',''),(23,'Siiri','Salajane','543334567','siiri.salajane@eesti.ee','FEMALE','ACTIVE',10.00,10.00,10.00,'table','siiris','123456','M',''),(24,'Uku','Suviste','543334567','uku.suviste@eesti.ee','MALE','ACTIVE',10.00,10.00,10.00,'table','UkuS','123456','U',''),(25,'Annika','Tasa','543334567','annika.tasa@eesti.ee','FEMALE','ACTIVE',10.00,10.00,10.00,'table','AnnikaT','123456','U','Lapsepuhkusel'),(26,'Mari','Maias','543334567','mari.maias@hot.ee','FEMALE','ACTIVE',10.00,10.00,10.00,'table','','123456','U','mega mega pikk mega mega pikk mega mega pikkmega mega pikk mega mega pikk mega mega pikk mega mega pikk mega mega pikkmega mega pikkmega mega pikk mega mega pikk'),(27,'Alo','Einla','543334567','alo.einla@hot.ee','MALE','ACTIVE',10.00,10.00,10.00,'table','AloE','123456','U',''),(28,'Juhan','Piip','543334567','juhan.piip@eesti.ee','MALE','ACTIVE',10.00,10.00,10.00,'table','JuhanP','123456','U','null');
/*!40000 ALTER TABLE `dancer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dancer_event`
--

DROP TABLE IF EXISTS `dancer_event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dancer_event` (
  `dancer_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `replacementDancer` int(11) DEFAULT NULL,
  `participationInfo` enum('YES','NO','MAYBE','NO_INFO') DEFAULT NULL,
  PRIMARY KEY (`dancer_id`,`event_id`),
  KEY `dancer_event_event_idx` (`event_id`),
  CONSTRAINT `dancer_event_dancer` FOREIGN KEY (`dancer_id`) REFERENCES `dancer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `dancer_event_event` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dancer_event`
--

LOCK TABLES `dancer_event` WRITE;
/*!40000 ALTER TABLE `dancer_event` DISABLE KEYS */;
INSERT INTO `dancer_event` VALUES (20,1,3,'NO'),(20,12,1,'YES'),(20,13,1313131313,'YES'),(20,14,12135436,'YES'),(20,15,1212121212,'YES'),(20,16,3333,'YES'),(20,17,3,'NO'),(20,18,3,'YES'),(20,36,1234567,'YES'),(20,38,0,'YES'),(20,40,0,'YES'),(22,12,0,'YES'),(22,13,0,'YES'),(22,15,0,'YES'),(22,16,0,'YES'),(22,17,0,'YES'),(22,18,0,'YES'),(22,36,0,'YES'),(22,37,0,'NO'),(23,17,777,'NO'),(23,18,777,'NO'),(23,39,0,'YES'),(24,16,0,'YES'),(24,36,0,'YES'),(24,41,0,'NO'),(24,42,0,'NO'),(25,16,0,'YES'),(25,36,0,'MAYBE'),(26,17,0,'YES'),(26,18,0,'YES'),(26,37,0,'YES'),(26,40,0,'YES'),(26,42,0,'MAYBE'),(27,17,0,'YES'),(27,36,0,'YES'),(27,39,0,'MAYBE'),(28,17,0,'YES'),(28,42,0,'YES');
/*!40000 ALTER TABLE `dancer_event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `eventName` varchar(200) NOT NULL DEFAULT 'Beerhouse',
  `dancerFee` decimal(5,2) NOT NULL,
  `eventDate` date NOT NULL,
  `comment` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (1,'nulllluuu',10.00,'2019-12-31','Bee boop'),(12,'Beerhouse',10.00,'2018-04-13','Beerhouses kell 0:00'),(13,'Beerhouse',10.00,'2018-04-14','Beerhouses kell 0:00'),(14,'Eriesinemine',25.00,'2017-06-14','Beerhouses kell 0:00'),(15,'Beerhouse',10.00,'2018-04-15','Beerhouses kell 0:00'),(16,'Beerhouse',15.00,'2018-05-13','Beerhouses kell 0:00'),(17,'Beerhouse',15.00,'2018-05-13','Beerhouses kell 0:00'),(18,'Beerhouse',15.00,'2018-07-01','Beerhouses kell 0:00'),(36,'Sünnipäev',134.00,'2018-05-08','Kommipidu'),(37,'beeee',12.00,'2018-05-17',''),(38,'3456',12.00,'2018-05-03','3434'),(39,'2121',12.00,'2018-05-29','212'),(40,'12',12.00,'2018-05-25','121'),(41,'12',230.00,'2018-05-13','12'),(42,'12',12.00,'2018-05-09','12');
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quarter_report`
--

DROP TABLE IF EXISTS `quarter_report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quarter_report` (
  `dancer_id` int(11) NOT NULL,
  `quarter_date` varchar(6) NOT NULL,
  `quarter_sum` decimal(6,2) DEFAULT '0.00',
  `prepayment` enum('YES','NO') NOT NULL DEFAULT 'NO',
  PRIMARY KEY (`dancer_id`,`quarter_date`),
  CONSTRAINT `quarter_report_dancer` FOREIGN KEY (`dancer_id`) REFERENCES `dancer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quarter_report`
--

LOCK TABLES `quarter_report` WRITE;
/*!40000 ALTER TABLE `quarter_report` DISABLE KEYS */;
INSERT INTO `quarter_report` VALUES (20,'2018-1',0.00,'NO'),(20,'2018-2',-496.04,'YES'),(20,'2018-3',-728.01,'YES'),(20,'2018-4',0.00,'NO'),(22,'2018-1',0.00,'NO'),(22,'2018-2',0.00,'NO'),(22,'2018-3',-659.67,'YES'),(22,'2018-4',0.00,'NO'),(23,'2018-1',0.00,'NO'),(23,'2018-2',0.00,'NO'),(23,'2018-3',-60.00,'YES'),(23,'2018-4',0.00,'NO'),(24,'2018-1',-60.00,'YES'),(24,'2018-2',91.99,'YES'),(24,'2018-3',-60.00,'YES'),(24,'2018-4',0.00,'NO'),(25,'2018-1',-60.00,'YES'),(25,'2018-2',-20.00,'YES'),(25,'2018-3',-60.00,'YES'),(25,'2018-4',0.00,'NO'),(26,'2018-1',0.00,'NO'),(26,'2018-2',-50.01,'YES'),(26,'2018-3',-50.01,'YES'),(26,'2018-4',0.00,'NO'),(27,'2018-1',0.00,'NO'),(27,'2018-2',-50.01,'YES'),(27,'2018-3',-60.00,'YES'),(27,'2018-4',0.00,'NO'),(28,'2018-1',0.00,'NO'),(28,'2018-2',-50.01,'YES'),(28,'2018-3',-60.00,'YES'),(28,'2018-4',0.00,'NO');
/*!40000 ALTER TABLE `quarter_report` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `report`
--

DROP TABLE IF EXISTS `report`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `report` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `year` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `dancer_id` int(11) NOT NULL,
  `report_sum` float DEFAULT NULL,
  PRIMARY KEY (`id`,`dancer_id`),
  KEY `report_dancer_idx` (`dancer_id`),
  CONSTRAINT `report_dancer` FOREIGN KEY (`dancer_id`) REFERENCES `dancer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `report`
--

LOCK TABLES `report` WRITE;
/*!40000 ALTER TABLE `report` DISABLE KEYS */;
/*!40000 ALTER TABLE `report` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-18 10:51:51
