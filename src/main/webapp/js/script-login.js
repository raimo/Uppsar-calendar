
	var authenticatedUser = null;

	function logInUser(){
		var username= $('#loginUsername').val();
		var password= $('#loginPassword').val();
		$.ajax(
		{

			url:'/uppsar/rest/authenticate_user',
			method: 'POST',
			data: { 
				'username': username,
				'password': password

			},
			complete: function(result){
				if (result.responseText== 'SUCCESS'){
					document.location="calendar.html";
				} else {
					$('#errorBox').css({'display': 'block'});
				}

			}
		});
	}

	function logOutUser() {

		$.ajax(
		{
			url:'/uppsar/rest/logout',
			method: 'GET',
			complete: function(result){

					document.location="index.html";
				$('#loginUserBox').show();
				$('#loginPasswordBox').show();
				$('#errorBox').css({'display': 'none'});

				
			}
			
		}

		);
	}

	function getAuthenticatedUser(loginFailAction){
		var firstName= "";
		var lastName = "";
			
		$.ajax(
		{

			url:'/uppsar/rest/get_authenticated_user',
			method: 'GET',
			complete: function(result){
				var user = result.responseJSON;
				authenticatedUser = user;
				firstName= user.firstName;
				lastName= user.lastName;
				if (user != null && user.id>0){
					$('#loggedInAs').empty;
					$('#loggedInAs').append(firstName + " " + lastName);
				
				} else {
					$('#loginUserBox').show();
					$('#loginPasswordBox').show();
					if (loginFailAction != null) {
						loginFailAction();
					}
				}

			}
		});
		

	}
	
	function redirectToIndex() {
		document.location = "index.html";
	}